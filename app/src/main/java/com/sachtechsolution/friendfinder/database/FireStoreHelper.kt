package com.sachtechsolution.friendfinder.database

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.sachtechsolution.friendfinder.model.User


//
class FireStoreHelper {
    private val firestoreRef by lazy {
        FirebaseFirestore.getInstance().apply {
            firestoreSettings = FirebaseFirestoreSettings.Builder()
                    .setPersistenceEnabled(true)
                    .build()
        }
    }

    companion object {
        private val fireStoreHelper by lazy { FireStoreHelper() }
        fun getInstance(): FireStoreHelper {
            return fireStoreHelper
        }
    }

    fun setUser(number: String?, user: User): Task<Void> {
        return getUserRef().document(number!!).set(user).addOnCompleteListener { }
    }

    fun getUser(number: String?): Task<DocumentSnapshot> {
        return getUserRef().document(number!!).get()
    }

    private fun getUserRef(): CollectionReference {
        return getStoreRef().collection("users")
    }

    fun getStoreRef(): FirebaseFirestore {
        return firestoreRef
    }

    fun getContactRef(): CollectionReference {
        return getStoreRef().collection("contacts")
    }

    fun getContactNameCount(contacts: String, name: String): Task<DocumentSnapshot> {
        return getStoreRef().collection("contacts").document(contacts).collection("names").document(name).get()
    }

    fun getContactName(contact: String): Task<QuerySnapshot> {
        return getContactRef().document(contact).collection("names").orderBy("count", Query.Direction.ASCENDING).get()
    }

    fun storeProfile(number: String, docData: Any){
    val future = getStoreRef().collection("users").document(number).set(docData)
    }

}