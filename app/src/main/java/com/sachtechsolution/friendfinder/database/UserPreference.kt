package com.sachtechsolution.friendfinder.database

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.sachtechsolution.friendfinder.model.User

//
class UserPreference(val activity: Activity) {
//creating sharepref object

    private var sharePref: SharedPreferences = activity.getSharedPreferences("user", Activity.MODE_PRIVATE)

    //save object by converting to gson
    fun setUser(user: User) {
        sharePref.edit().putString("userdata", Gson().toJson(user).toString()).commit()
    }

    fun getUser(): User {
        return Gson().fromJson(sharePref.getString("userdata", ""), User::class.java)
    }

    fun hasUser(): Boolean {
        return !sharePref.getString("userdata", "").isNullOrEmpty()
    }

    fun setUserId(token: String?) {

        sharePref.edit().putString("userID", token).commit()
    }

    fun getUserId(): String {
        return sharePref.getString("userID", "")
    }

    fun clearpref() {
        sharePref.edit().clear().commit()
    }

}
