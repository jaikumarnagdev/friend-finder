package com.sachtechsolution.friendfinder

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.ConnectivityManager
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.sachtechsolution.friendfinder.basepackage.controller.App
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.realm.RealmController
import com.sachtechsolution.friendfinder.ui.home.call_logs.model.LogBean
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import com.sachtechsolution.friendfinder.ui.home.friendList.model.FriendListBean
import com.wickerlabs.logmanager.LogsManager
import io.realm.RealmResults
import io.realm.Sort
import java.util.*


fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.showLog(message: String, title: String) {
    Log.e(title, message)
}

inline fun <reified T> Context.openActivity() {
    startActivity(Intent(this, T::class.java))
}

fun Context.storeContactsinRealm(contactsBeas: ContactsBean) {
    var realm = RealmController().realm
    realm.beginTransaction()
    realm.insertOrUpdate(contactsBeas)
    realm.commitTransaction()
    realm.close()
}

fun Context.storeFriendListToRealm(contactsBeas: FriendListBean) {
    var realm = RealmController().realm
    realm.beginTransaction()
    realm.insertOrUpdate(contactsBeas)
    realm.commitTransaction()
    realm.close()
    OttoBus.bus.post(contactsBeas)
}

fun Context.searchName(number: String): RealmResults<ContactsBean> {
    var realm = RealmController().realm
    val data = realm.where(ContactsBean::class.java)
            .contains("number", number)
            .findAll()
    realm.close()
    return data

}

fun Context.storeContacts(contactsBeas: ArrayList<ContactsBean>) {
    var realm = RealmController().realm
    realm.beginTransaction()
    realm.insertOrUpdate(contactsBeas)
    realm.commitTransaction()
    realm.close()
}

fun Context.storeContacts(contactsBeas: ContactsBean) {
    var realm = RealmController().realm
    realm.beginTransaction()
    realm.insertOrUpdate(contactsBeas)
    realm.commitTransaction()
    realm.close()
}

fun Context.getContacts(): ArrayList<ContactsBean> {
    var contactList = ArrayList<ContactsBean>()
    var contactNumber = ""
    var cursor: Cursor? = null
    var contactName = ""
    var bean: ContactsBean
    var contactImage = ""
    val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
    val selection = ContactsContract.Contacts.HAS_PHONE_NUMBER

    cursor = contentResolver.query(uri, arrayOf<String>(ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME), selection, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")

    if (cursor.count > 0) {
        cursor.moveToFirst()
        do {
            contactNumber = cursor!!.getString(cursor!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
            contactName = cursor!!.getString(cursor!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            contactImage = "noImage"
            bean = ContactsBean(contactName, contactImage, contactNumber.replace("-", "").replace(" ", ""))
            contactList.add(bean)
        } while (cursor.moveToNext())

    }
    cursor!!.close()
    return contactList
}

fun isPermissionsGranted(context: Context, p0: Array<String>): Boolean {
    p0.forEach {
        if (ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_DENIED)
            return false
    }
    return true
}

fun askForPermissions(activity: Activity, array: Array<String>) {
    ActivityCompat.requestPermissions(activity, array, 0)
}

fun isNetConnected(): Boolean {
    val cm = App.getApp().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

fun Context.showConfirmAlert(message: String?, positiveText: String?
                             , negativetext: String?
                             , onConfirmed: () -> Unit = {}
                             , onCancel: () -> Unit = { }) {

    if (message.isNullOrEmpty()) return

    val builder = AlertDialog.Builder(this)

    builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(positiveText) { dialog, id ->
                //do things

                onConfirmed.invoke()
                dialog.dismiss()
            }
            .setNegativeButton(negativetext) { dialog, id ->
                //do things

                onCancel.invoke()
                dialog.dismiss()
            }

    val alert = builder.create()
    alert.show()
    alert.getButton(Dialog.BUTTON_NEGATIVE).setAllCaps(false)
    alert.getButton(Dialog.BUTTON_POSITIVE).setAllCaps(false)


}

fun Context.getAllContacts(): MutableList<ContactsBean>? {
    var realm = RealmController().realm
    val dd = realm.where(ContactsBean::class.java).sort("name", Sort.ASCENDING).findAll()

    return realm.copyToRealmOrUpdate(dd)

}

fun Context.getAllFriendList(): MutableList<FriendListBean>? {
    var realm = RealmController().realm
    val dd = realm.where(FriendListBean::class.java).sort("name", Sort.ASCENDING).findAll()
    realm.copyToRealmOrUpdate(dd)
    realm.close()
    return dd

}

fun Context.getLogFromRealm(): MutableList<LogBean>? {
    var realm = RealmController().realm
    realm.refresh()
    val dd = realm.where(LogBean::class.java).sort("callDate", Sort.DESCENDING).findAll()

    val data = realm.copyToRealmOrUpdate(dd)
    return data

}

fun Context.storeLogToRealm(list: LogBean) {
    var realm = RealmController().realm
    realm.beginTransaction()
    realm.insertOrUpdate(list)
    realm.commitTransaction()
    realm.close()
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Context.getLogsFromPhone() {
    val mdate = Date()
    val logsManager = LogsManager(this)
    val callLogs = logsManager.getLogs(LogsManager.ALL_CALLS)
    callLogs.reverse()
    for (i in 0 until callLogs.size) {
        var name = callLogs[i].contactName.toString()
        var number = callLogs[i].number.toString()
        var date = callLogs[i].date
        var duration = callLogs[i].duration
        var bean = LogBean(name, date, duration, number)
        storeLogToRealm(bean)

    }
}
