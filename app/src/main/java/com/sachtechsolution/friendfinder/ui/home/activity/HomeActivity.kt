package com.sachtechsolution.friendfinder.view.ui.home

import android.os.Bundle
import android.os.Handler
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import com.sachtechsolution.friendfinder.*
import com.sachtechsolution.friendfinder.basepackage.base.BaseActivity
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.ui.home.call_logs.ui.CallFragment
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import com.sachtechsolution.friendfinder.ui.home.contacts.ui.ContactsFragment
import com.sachtechsolution.friendfinder.ui.home.friendList.ui.FriendListFragment
import com.sachtechsolution.friendfinder.ui.home.intracter.HomeActivityIntracter
import com.sachtechsolution.friendfinder.ui.home.map.ui.MapFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity() , HomeActivityIntracter {
    var index = 1

    override fun updateToolbar(b: Boolean) {
        if (b) {
            showViews()
        }
        else
            hideViews()
    }
    private fun hideViews() {
        toolbar.animate().translationY((-toolbar.height).toFloat()).interpolator = AccelerateInterpolator(2F)
         toolbar.gone()
          }
    private fun showViews() {
        toolbar.animate().translationY(0F).interpolator = DecelerateInterpolator(2F)
        toolbar.visible()
    }
    override fun fragmentContainer(): Int {
        return R.id.homePageContainer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        listner = this
        Handler().run {
            searchNamae()
        }

        bottomNavigationBar = this.findViewById(R.id.bottom_bar)
        navigator.replaceFragment(CallFragment::class.java, true)
        setBottomBar()

    }

    private fun setBottomBar() {
        val itemOne = BottomBarItem(R.drawable.call, R.string.calls)
        val itemTwo = BottomBarItem(R.drawable.contact, R.string.contacts)
        val itemThree = BottomBarItem(R.drawable.map, R.string.map)
        val itemFour = BottomBarItem(R.drawable.friend_list, R.string.friendList)

        bottomNavigationBar!!.addTab(itemOne)
        bottomNavigationBar!!.addTab(itemTwo)
        bottomNavigationBar!!.addTab(itemThree)
        bottomNavigationBar!!.addTab(itemFour)
        bottomNavigationBar!!.setOnSelectListener { position -> doStuff(position) }
    }

    private fun doStuff(position: Int) {
        when (position) {
            0 -> navigator.replaceFragment(CallFragment::class.java, true)
            1 -> navigator.replaceFragment(ContactsFragment::class.java, true)
            2 -> navigator.replaceFragment(MapFragment::class.java, true)
            3 -> navigator.replaceFragment(FriendListFragment::class.java, true)
        }
    }

    companion object {

        var bottomNavigationBar: BottomNavigationBar? = null
        var listner: HomeActivityIntracter? = null
        fun updateSelectedPos(pos: Int) {

            if (bottomNavigationBar != null)
                bottomNavigationBar!!.selectTab(pos, false)

        }
    }

    private fun searchNamae() {

        if (index < getLogFromRealm()!!.size) {
            val callername = getLogFromRealm()!![index].callername
            if (callername.matches(".*\\d+.*".toRegex())) {
                getFriendList(callername, index)
            } else {
                this.index = index.plus(1)
                searchNamae()
            }

        } else {
            showToast("done")
        }

    }

    private fun getFriendList(number: String, index: Int) {
        var number = number

        if (!number.contains("+")) {
            number = "+91$number"
        }

        FireStoreHelper.getInstance().getContactName(number).addOnCompleteListener {
            if (it.isSuccessful) {
                var name = ""
                if (it.result.documents.size > 0) {
                    it.result.documents.forEach {
                        name = it.id
                        showToast(name)
                        OttoBus.bus.post("$name,$index")
                        this.index = index.plus(1)
                        var bean = ContactsBean(name, "noImage", number.replace("-", "").replace(" ", ""))
                        storeContacts(bean)
                        searchNamae()
                    }
                } else {
                    this.index = index.plus(1)
                    searchNamae()
                }
            } else {
                this.index = index.plus(1)
                searchNamae()
            }

        }

    }
}
