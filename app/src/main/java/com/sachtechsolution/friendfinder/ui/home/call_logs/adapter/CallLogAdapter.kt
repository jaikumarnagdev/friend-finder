package com.sachtechsolution.friendfinder.ui.home.call_logs.adapter

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.friendfinder.searchName
import com.sachtechsolution.friendfinder.ui.home.call_logs.model.LogBean
import com.sachtechsolution.friendfinder.ui.home.viewProfile.ViewProfileFragment
import kotlinx.android.synthetic.main.log_layout.view.*
import java.text.DateFormat
import java.util.*

class CallLogAdapter(val context: Context, var bean: MutableList<LogBean>?, val baseAcitivityListener: BaseAcitivityListener) : RecyclerView.Adapter<CallLogAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.log_layout, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val log = bean!![position]
        val date1 = Date(log.callDate)
        val dateFormat = DateFormat.getDateTimeInstance(DateFormat.ERA_FIELD, DateFormat.SHORT)

        if(log.callername.contains("-?\\d+(\\.\\d+)?".toRegex()))

                    if (context.searchName( log!!.callername)!!.size>0)
                    holder.itemView.phoneNum.text = context.searchName( log!!.callername)!![0]?.name
                else
                        holder.itemView.phoneNum.text = log!!.callername

        else
            holder.itemView.phoneNum.text = log!!.callername

        holder.itemView.callDuration.text = secToTime(log!!.callDuration)+" sec"
        holder.itemView.callDate.text = dateFormat.format(date1)

        holder.itemView.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(ViewProfileFragment::class.java,false, Bundle().apply { putString("number",log!!.callername) })
        }

        /*when(log!!.type) {

            LogsManager.INCOMING->
                holder.itemView.callImage.setImageResource(R.drawable.received)
            LogsManager.OUTGOING->
                holder.itemView.callImage.setImageResource(R.drawable.sent)
            LogsManager.MISSED->
                holder.itemView.callImage.setImageResource(R.drawable.missed)


            else->
            holder.itemView.callImage.setImageResource(R.drawable.cancelled)

        }*/
    }


    override fun getItemCount(): Int {
        return bean!!.size
    }

       private fun secToTime(sec: Int): String {
           val second = sec % 60
           var minute = sec / 60
           if (minute >= 60) {
               val hour = minute / 60
               minute %= 60
               return hour.toString() + ":" + (if (minute < 10) "0$minute" else minute) + ":" + if (second < 10) "0$second" else second
           }
           return minute.toString() + ":" + if (second < 10) "0$second" else second
       }

    fun updatData(name: String, index: String) {

        bean!![index.toInt()].callername=name
        notifyItemInserted(index.toInt())
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
