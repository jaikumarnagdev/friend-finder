package com.sachtechsolution.friendfinder.ui.home.friendList.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.ui.home.friendList.model.FriendListBean
import kotlinx.android.synthetic.main.view_friendlist.view.*

class FriendListAdapter(activity: Context, list: MutableList<FriendListBean>) : RecyclerView.Adapter<FriendListAdapter.MyViewHolder>() {

    internal var context: Context = activity
    var list: MutableList<FriendListBean>?=list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_friendlist, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.friendName.text = list!![position]!!.name
        holder.itemView.friendNumber.text = list!![position]!!.mobile
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    fun updateData(list: ArrayList<FriendListBean>) {

        this.list= list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
