package com.sachtechsolution.friendfinder.ui.home.map.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.marker_dialog.view.*

class MapFragment : BaseFragment(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks {
    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)
        if (mLocation != null) {
            var mLat = 30.7046//mLocation.latitude
            var mLog = 76.7179//mLocation.longitude
            var mLatLong = LatLng(mLat, mLog)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLong, 12f))
            mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
                override fun getInfoContents(p0: Marker?): View? {
                    return null
                }

                override fun getInfoWindow(p0: Marker?): View {
                    var view = LayoutInflater.from(activity).inflate(R.layout.marker_dialog, null)
                    view.mark_title.text = p0!!.title
                    return view
                }
            })
            for (i in 0..5) {

                var mLatLong = LatLng(mLat, mLog)
                Picasso.with(activity!!).load("http://www.gstatic.com/webp/gallery/1.jpg").resize(100, 100).centerCrop()
                        .into(object : Target {
                            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                            }

                            override fun onBitmapFailed(errorDrawable: Drawable?) {
                            }

                            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                                val output = Bitmap.createBitmap(bitmap!!.width,
                                        bitmap!!.height, Bitmap.Config.ARGB_8888)
                                val canvas = Canvas(output)
                                val color = -0xbdbdbe
                                val paint = Paint()
                                val rect = Rect(0, 0, bitmap.width, bitmap.height)
                                paint.isAntiAlias = true
                                canvas.drawARGB(0, 0, 0, 0)
                                paint.color = color
                                canvas.drawCircle((bitmap.width / 2).toFloat(), (bitmap.height / 2).toFloat(),
                                        (bitmap.width / 2).toFloat(), paint)
                                paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
                                canvas.drawBitmap(bitmap, rect, rect, paint)
                                uMarker = mMap.addMarker(MarkerOptions().position(mLatLong).title("move")
                                        .draggable(false).zIndex(1.0f).icon(BitmapDescriptorFactory.fromBitmap(output)))
                            }
                        })
                mLat += 1
                mLog += 1
            }

            /*mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener{
                override fun onMarkerDragEnd(p0: Marker?) {
                    val lat=uMarker.position.latitude
                    val long=uMarker.position.longitude
                    val geocoder= Geocoder(activity)
                    var address = geocoder.getFromLocation(lat, long, 1)

                    Toast.makeText(activity," loc:"+ address[0].countryName+" "+address[0].adminArea+" "+address[0].locality,Toast.LENGTH_SHORT).show()
                }
                override fun onMarkerDragStart(p0: Marker?) {
                }
                override fun onMarkerDrag(p0: Marker?) {
                }
            })*/
        }
    }

    fun loadBitmapFromView(v: View): Bitmap? {
        if (v.getMeasuredHeight() <= 0) {
            v.measure(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            val b = Bitmap.createBitmap(/*v.getMeasuredWidth()*/60, /*v.getMeasuredHeight()*/60, Bitmap.Config.ARGB_8888);
            val c = Canvas(b)
            v.layout(0, 0, 60, 60);
            v.draw(c);
            return b;
        }
        return null
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_map2
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    lateinit var uMarker: Marker
    private var myMap: SupportMapFragment? = null

    private var markerView: View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        myMap = childFragmentManager.findFragmentById(R.id.location_fragment) as SupportMapFragment
        myMap?.getMapAsync(this)
        mGoogleClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleClient.connect()
        markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)
    }

    fun isLocationEnabled() {
        val locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        } else {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }
}
