package com.sachtechsolution.friendfinder.ui.home.friendList.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FriendListBean extends RealmObject {
    String city;
    String lat;
    String lon;

    @PrimaryKey
    String mobile;
    String name;
    String profile;
    String state;

public FriendListBean(){

}
    public FriendListBean(String name, String city, String lat, String lon, String mobile, String profile, String state){
        this.name=name;
        this.city=city;
        this.lat=lat;
        this.lon=lon;
        this.mobile=mobile;
        this.profile=profile;
        this.state=state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
