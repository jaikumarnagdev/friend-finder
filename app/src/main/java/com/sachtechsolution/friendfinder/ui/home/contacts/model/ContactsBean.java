package com.sachtechsolution.friendfinder.ui.home.contacts.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ContactsBean extends RealmObject {

    String number;
    @PrimaryKey
    String name;
    String image;


    public ContactsBean() {

    }

    public ContactsBean(String contactname, String contactImage, String contactNumber) {
        this.name = contactname;
        this.number = contactNumber;
        this.image = contactImage;

    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNumber() {
        return number;
    }
}
