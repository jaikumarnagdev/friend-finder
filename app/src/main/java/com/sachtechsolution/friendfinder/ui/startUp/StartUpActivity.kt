package com.sachtechsolution.friendfinder.ui.startUp

import android.Manifest
import android.database.Cursor
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.util.Log
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.askForPermissions
import com.sachtechsolution.friendfinder.basepackage.base.BaseActivity
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.isPermissionsGranted
import com.sachtechsolution.friendfinder.model.NameCount


class StartUpActivity : BaseActivity() {
    override fun fragmentContainer(): Int {
        return 0
    }

    private val REQUEST_CODE: Int = 123
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)

        Handler().postDelayed({
            if (isPermissionsGranted(this, arrayOf(Manifest.permission.READ_CONTACTS))) {
                getContacts()
            } else {
                askForPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS))
            }
        }, 3000)

    }

    private fun getContacts() {
//        showProgress()
        val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
        UploadContacts(phones).execute()
    }

    inner class UploadContacts(val phones: Cursor) : AsyncTask<Void, Void, Void>() {
        private var contactsCount: Int = 0
        private var contactsFirebaseCount: Int = 0
        val batch = fireStoreHelper.getStoreRef().batch()
        override fun onPreExecute() {
            super.onPreExecute()

        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
           finish()
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            while (phones.moveToNext()) {
                var name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                var phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                Log.e("name>>", "$name  $phoneNumber")
                Log.e(">>", android.util.Patterns.PHONE.matcher(phoneNumber).matches().toString())
                if (phoneNumber.length in 11..14 && !name.isNullOrEmpty()) {
                    phoneNumber = phoneNumber.replace("-", "")
                    phoneNumber = phoneNumber.replace(" ", "")
                    name = name.replace("/", "")
                    if (!phoneNumber.contains("+91") && phoneNumber.length == 10)
                        phoneNumber = "+91$phoneNumber"
                    var count = 0
                    contactsCount++
                    fireStoreHelper.getContactNameCount(phoneNumber, name)
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    val result = it.result
                                    if (result.exists()) {
                                        count = result.toObject(NameCount::class.java)!!.count++
                                    }
                                    contactsFirebaseCount++
                                    batch.set(fireStoreHelper.getContactRef().document(phoneNumber).collection("names").document(name), NameCount(count = count))
                                    if (contactsCount == contactsFirebaseCount) {
                                        batch.commit().addOnCompleteListener {
                                            if (it.isComplete) {
                                                hideProgress()
                                            }
                                        }
                                    }
                                }
                            }

                }
            }
            return null
        }

    }
}