package com.sachtechsolution.friendfinder.ui.startUp

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.ui.callDialog.CallDialogActivity

class EmptyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty)
        val callIntent=Intent(this,CallDialogActivity::class.java)
        callIntent.putExtra("no", intent.getStringExtra("no"))
        startActivity(callIntent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask()
        }
    }
}
