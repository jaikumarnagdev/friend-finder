package com.sachtechsolution.friendfinder.ui.home.contacts.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import kotlinx.android.synthetic.main.view_call_log.view.*
import kotlinx.android.synthetic.main.view_contacts.view.*

class ContactAdapter(activity: Context, var bean: ArrayList<ContactsBean>) : RecyclerView.Adapter<ContactAdapter.MyViewHolder>() {

    internal var context: Context = activity


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_contacts, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.contactName.text = bean[position].name
        holder.itemView.contactNumber.text=bean[position].number
        if (!bean[position].image.equals("noImage")){
            Glide.with(context).load(bean[position].image).into(holder.itemView.contactUserPic)
        }
    }

    override fun getItemCount(): Int {
        return bean.size
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
