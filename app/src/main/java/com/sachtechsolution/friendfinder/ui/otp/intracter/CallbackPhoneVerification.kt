package com.sachtechsolution.friendfinder.ui.otp.intracter

import com.google.firebase.auth.PhoneAuthProvider

interface CallbackPhoneVerification {
    fun invalidCode()
    fun verifiedSuccessfully()
    fun messageSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken)

    fun autoReterivalCode(code: String?)
    fun verificationfailed(message: String)
    fun otpRequestFailed(message: String)
    fun verificationfailed(resultCode: Int)
}
