package com.sachtechsolution.friendfinder.ui.otp.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.View
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.database.UserPreference
import com.sachtechsolution.friendfinder.model.User
import com.sachtechsolution.friendfinder.view.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_signupinfo.*

class SignUpInfoFragment :BaseFragment(), GoogleApiClient.ConnectionCallbacks {
    var mGoogleClient: GoogleApiClient? = null
    var lat: Double?=null
    var long: Double?=null
    lateinit var mLocation : Location

    override fun onConnectionSuspended(p0: Int) {
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation= LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)

        if(mLocation!=null) {
            lat = mLocation.latitude
            long = mLocation.longitude
            showToast("LatLong:$lat/$long") }
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_signupinfo
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var mobile=arguments!!.getString("number")
        pMobile.setText(mobile)
        arguments = arguments
        mGoogleClient=GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()


        pContinue.setOnClickListener {

                if (networkAvaileble()){
                    showProgress()
                    doStuff() }

                else
                    showToast("check internet connection")

        }
    }



    private fun doStuff() {
        var name=pName.text.toString()
        var mobile=pMobile.text.toString()
        var city=pCity.text.toString()
        var profile=pProfile.text.toString()
        var state=pState.text.toString()
        var lat=lat
        var long=long

        if (name!!.isEmpty()||mobile!!.isEmpty()||city!!.isEmpty()||profile!!.isEmpty()||state!!.isEmpty())
            showToast("all field required")
        else{

            var user=User(name=name,mobile = mobile,lat = lat.toString(),long = long.toString(),state = state,city = city,profile = profile)

            FireStoreHelper.getInstance().setUser(mobile,user).addOnCompleteListener(){

                if (it.isSuccessful) {
                        UserPreference(activity!!).setUser(user)
                        baseAcitivityListener.navigator.openActivity(HomeActivity::class.java)
                        activity?.finish()
                        showToast("completed")
                        hideProgress() }

                else {
                    showToast("cancel")
                    hideProgress() }

            }

        }

    }


    override fun onStart() {
        checkLoc()
        super.onStart()
    }


    override fun onStop() {
        mGoogleClient!!.disconnect();
        super.onStop()
    }


    private fun checkLoc() {
        //if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        val locationManager =activity!!. getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            mGoogleClient!!.connect()
        else
        {
            val intent = Intent()
            intent.action = Settings.ACTION_LOCATION_SOURCE_SETTINGS
            startActivityForResult(intent, 1)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       showToast("enabaled")
    }
}
