package com.sachtechsolution.friendfinder.ui.otp.ui

import android.os.Bundle
import android.view.View
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_mobilenumber.*
import kotlinx.android.synthetic.main.fragment_add_mobilenumber.view.*

class FragmentAddNumber: BaseFragment(){

    override fun viewToCreate(): Int {
      return  R.layout.fragment_add_mobilenumber
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.verify_next_btn.setOnClickListener {
            //baseAcitivityListener.navigator.openActivity(HomeActivity::class.java)
            if (networkAvaileble()) {
                    if (!verify_phone_number_edt.text.isEmpty()) {
                        openOtpFragment(verify_phone_number_edt.text.toString())
                    } else verify_phone_number_edt.error = "Please Enter your Mobile Number"
            }else{
                showToast("Check network connection")
            }
        }
    }

    private fun openOtpFragment(number: String) {
        val numberBundle = Bundle()
            numberBundle.putString("number", verify_phonenumer_cc_edt.selectedCountryCodeWithPlus+number)
           baseAcitivityListener.navigator.replaceFragment(FragmentOtp::class.java, true,numberBundle)
    }
}