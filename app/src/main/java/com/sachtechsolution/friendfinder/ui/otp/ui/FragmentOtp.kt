package com.sachtechsolution.friendfinder.ui.otp.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import com.sachtechsolution.friendfinder.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.PhoneAuthProvider
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.ui.otp.intracter.CallbackPhoneVerification
import com.sachtechsolution.friendfinder.ui.otp.intracter.FirebasePhoneVerification
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_otp.*
import com.goodiebag.pinview.Pinview
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.database.UserPreference
import com.sachtechsolution.friendfinder.model.User
import com.sachtechsolution.friendfinder.view.ui.home.HomeActivity
import java.util.concurrent.TimeUnit


class FragmentOtp: BaseFragment(), CallbackPhoneVerification {
    private var verificationId: String = ""
    var number:String?=null
    var countDownTimer:CountDownTimer?=null
    private var token: PhoneAuthProvider.ForceResendingToken? = null
    val firestore by lazy { FireStoreHelper.getInstance() }

    override fun invalidCode() {
        hideProgress()
    }

    override fun verifiedSuccessfully() {
        //hideProgress()
        countDownTimer?.cancel()
        showToast("user loged in")
        firestore.getUser(number).addOnCompleteListener {
            if (it.isSuccessful){
                if(it.result.exists()){
                    UserPreference(activity!!).setUser(it.result.toObject(User::class.java)!!)
                    baseAcitivityListener.navigator.openActivity(HomeActivity::class.java)
                    activity?.finish()
                }
                else{
                    val numberBundle = Bundle()
                    numberBundle.putString("number", number)
                    baseAcitivityListener.navigator.replaceFragment(SignUpInfoFragment::class.java,false,numberBundle)
                }
            }
        }

    }

    override fun messageSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
        this.verificationId = verificationId
        this.token = token
    }

    override fun autoReterivalCode(code: String?) {
        showToast(code)
    }

    override fun verificationfailed(message: String) {
        hideProgress()
        showToast(message)
    }

    override fun otpRequestFailed(message: String) {
        showToast(message)
    }

    override fun verificationfailed(resultCode: Int) {
        hideProgress()
        showToast(resultCode.toString())
    }

    private val firebasePhoneVerification by lazy { FirebasePhoneVerification(this!!.activity!!, this) }

    override fun viewToCreate(): Int {
        FirebaseApp.initializeApp(activity)
       return R.layout.fragment_otp
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OttoBus.bus.register(this)
        number = arguments?.getString("number")
        setclickListner()
        sendOtp()
        setupOtpEditTexts()

    }

    private fun sendOtp() {

        if(!number!!.contains("+"))
            number= "+91$number"
        firebasePhoneVerification.startPhoneNumberVerification(number!!)
        startTimer(60000)

    }

    @Subscribe
    fun getMessage(otp:String) {
        pinview.value = otp
    }

   fun confirmOtp(otp: String) {
        showProgress()
        firebasePhoneVerification.verifyPhoneNumberWithCode(verificationId, otp)
    }
    private fun setupOtpEditTexts() {
        pinview.setPinBackgroundRes(R.drawable.sample_background)
        pinview.pinLength=6
        pinview.pinHeight = 60
        pinview.pinWidth = 60
        pinview.inputType = Pinview.InputType.NUMBER

        pinview.setPinViewEventListener { pinview, fromUser ->
            //Make api calls here or what not
            hideKeyboard(activity)
            confirmOtp(pinview.value)
        }

    }


    private fun setclickListner() {
        optBackButton.setOnClickListener {
            activity?.onBackPressed()
            countDownTimer?.cancel()
        }
        otpChangeNumberButton.setOnClickListener {
            activity?.onBackPressed()
            countDownTimer?.cancel()
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onDetach() {
        super.onDetach()
        countDownTimer?.cancel()
        OttoBus.bus.unregister(this)
    }

    private fun startTimer(noOfMinutes: Int) {

        countDownTimer = object : CountDownTimer(noOfMinutes.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val hms = String.format("%02d:%02d",  TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))
                otpTimer.text = "RESEND IN $hms"//set text
            }
            override fun onFinish() {
                showToast("Resent code")
                startTimer(60000)
            }
        }.start()
    }
}