package com.sachtechsolution.friendfinder.ui.home.call_logs.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pedro.library.AutoPermissions
import com.sachtechsolution.friendfinder.*
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.getLogFromRealm
import com.sachtechsolution.friendfinder.getLogsFromPhone
import com.sachtechsolution.friendfinder.storeLogToRealm
import com.sachtechsolution.friendfinder.ui.home.call_logs.adapter.CallLogAdapter
import com.sachtechsolution.friendfinder.view.ui.home.HomeActivity
import com.squareup.otto.Subscribe
import im.dlg.dialer.DialpadActivity
import im.dlg.dialer.DialpadFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_call.*
import com.sachtechsolution.friendfinder.getLogsFromPhone
import com.sachtechsolution.friendfinder.storeLogToRealm
class CallFragment : BaseFragment(), DialpadFragment.Callback {
    var adapter: CallLogAdapter? = null
    override fun ok(formatted: String?, raw: String?) {
        showToast("$formatted $raw")
    }

    private var logsRunnable: Runnable? = null


    override fun viewToCreate(): Int {
        return R.layout.fragment_call

    }

    override fun onResume() {
        super.onResume()
        LogOperation().execute()
        HomeActivity.updateSelectedPos(0)
        adapter = CallLogAdapter(activity!!, activity!!.getLogFromRealm(), baseAcitivityListener)
        callLogRecyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        OttoBus.bus.unregister(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AutoPermissions.loadAllPermissions(this!!.activity!!, 1)
        OttoBus.bus.register(this)

        //activity!!.startService(Intent(activity,MyService::class.java))
        // baseAcitivityListener.navigator.openActivity(CallLogActivity::class.java)

        dialerView.setOnClickListener {
            val intent = Intent(context, DialpadActivity::class.java)
            intent.putExtra(DialpadActivity.EXTRA_RESULT_FORMATTED, DialpadActivity.EXTRA_RESULT_FORMATTED)
            startActivityForResult(intent, 100) // any result request code is ok
        }

        callLogRecyclerView.setOnScrollListener(object : HidingScrollListener() {
            override fun onHide() {
                listner!!.updateToolbar(false)
            }

            override fun onShow() {
                listner!!.updateToolbar(true)
            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            var formatted = data!!.getStringExtra(DialpadActivity.EXTRA_RESULT_FORMATTED)
            var number = data.getStringExtra(DialpadActivity.EXTRA_RESULT_RAW)
            callPlease(number)
        }
    }

    @Subscribe
    fun getMessage(data: String) {
        val words = data.split(",")
        var name = words[0]
        var index = words[1]
        /* adapter!!.updatData(name,index)
         adapter!!.notifyDataSetChanged()*/
    }

    private fun callPlease(number: String) {

        var intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        startActivity(intent)
    }

    private inner class LogOperation() : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg p0: Void?): Void? {
           activity!!.getLogsFromPhone()
            return null
        }

        override fun onPostExecute(result: Void?) {

            if (adapter !== null && callLogRecyclerView !== null) {
                adapter = CallLogAdapter(activity!!, activity!!.getLogFromRealm(), baseAcitivityListener)
                callLogRecyclerView.adapter = adapter
            }
        }

    }
}