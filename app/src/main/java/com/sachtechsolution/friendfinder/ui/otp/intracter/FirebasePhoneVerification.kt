package com.sachtechsolution.friendfinder.ui.otp.intracter

import android.app.Activity
import android.util.Log
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit

class FirebasePhoneVerification(private val activity: Activity, private val callback: CallbackPhoneVerification?) {
    internal var firebaseAuth: FirebaseAuth
    private val mResendToken: PhoneAuthProvider.ForceResendingToken? = null
    private var mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks? = null

    init {

        firebaseAuth = FirebaseAuth.getInstance()
        initCallback()
    }

    fun initCallback() {
        mCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {

                callback?.autoReterivalCode(credential.smsCode)
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                if (e is FirebaseAuthInvalidCredentialsException)
                    callback?.otpRequestFailed("Enter valid Number")
                else
                    callback?.verificationfailed(e.message.toString())
            }

            override fun onCodeSent(verificationId: String?,
                                    token: PhoneAuthProvider.ForceResendingToken?) {
                callback?.messageSent(verificationId!!, token!!)
            }
        }
    }

    fun startPhoneNumberVerification(phoneNumber: String) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, // Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                activity, // Activity (for callback binding)
                mCallbacks!!)        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

    }

    fun verifyPhoneNumberWithCode(verificationId: String, code: String) {
        // [START verify_with_code]
        try {
            if(!verificationId.isNullOrEmpty()){
                val credential = PhoneAuthProvider.getCredential(verificationId, code)
                signInWithPhoneAuthCredential(credential)
            }
        }
        catch (e:Exception){
            Log.e("###",e.toString())
            // callback?.verificationfailed("enter correct number")
        }
        // [END verify_with_code]





    }

    // [START resend_verification]
    fun resendVerificationCode(phoneNumber: String,
                               token: PhoneAuthProvider.ForceResendingToken) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, // Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                activity, // Activity (for callback binding)
                mCallbacks!!, // OnVerificationStateChangedCallbacks
                token)             // ForceResendingToken from callbacks
    }

    fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        callback!!.verifiedSuccessfully()

                    } else {

                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            callback!!.invalidCode()

                        }else  {

                            callback!!.verificationfailed(task.exception!!.localizedMessage)
                        }

                    }


                }
    }

}
