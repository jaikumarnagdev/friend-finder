package com.sachtechsolution.friendfinder.ui.home.contacts.ui

import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.widget.RecyclerView
import android.view.View
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import com.sachtechsolution.friendfinder.getAllContacts
import com.sachtechsolution.friendfinder.ui.home.contacts.adapter.ContactAdapter
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import com.sachtechsolution.friendfinder.ui.startUp.StartUpActivity
import com.sachtechsolution.friendfinder.view.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_call.*
import kotlinx.android.synthetic.main.fragment_contacts.*

class ContactsFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_contacts
    }

    override fun onResume() {
        super.onResume()
        HomeActivity.updateSelectedPos(1)
        var adapter = ContactAdapter(this!!.activity!!, activity!!.getAllContacts() as ArrayList<ContactsBean>)
        contactRecyclerView.adapter = adapter

        contactRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx:Int, dy:Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0)
                {
                    // Scrolling up
                    listner!!.updateToolbar(false)
                }
                else
                {
                    listner!!.updateToolbar(true)
                }
            }
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*upload.setOnClickListener {
            baseAcitivityListener.navigator.openActivity(StartUpActivity::class.java)
        }*/
    }


}