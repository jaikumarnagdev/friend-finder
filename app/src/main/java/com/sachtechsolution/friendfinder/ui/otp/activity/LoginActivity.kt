package com.sachtechsolution.friendfinder.ui.otp.activity

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import com.pedro.library.AutoPermissions
import com.pedro.library.AutoPermissionsListener
import com.sachtechsolution.friendfinder.*
import com.sachtechsolution.friendfinder.basepackage.base.BaseActivity
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.services.GetSavedName
import com.sachtechsolution.friendfinder.services.StoreNaameFromServer
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import com.sachtechsolution.friendfinder.ui.otp.ui.FragmentAddNumber


class LoginActivity : BaseActivity() {


    override fun fragmentContainer(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        getNavigator().replaceFragment(FragmentAddNumber::class.java, true)
    }





}
