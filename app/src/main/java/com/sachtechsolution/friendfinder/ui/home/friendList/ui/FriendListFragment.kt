package com.sachtechsolution.friendfinder.ui.home.friendList.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.sachtechsolution.friendfinder.HidingScrollListener
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.basepackage.base.BaseFragment
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.database.UserPreference
import com.sachtechsolution.friendfinder.getAllFriendList
import com.sachtechsolution.friendfinder.ui.home.friendList.adapter.FriendListAdapter
import com.sachtechsolution.friendfinder.ui.home.friendList.model.FriendListBean
import com.sachtechsolution.friendfinder.ui.otp.activity.LoginActivity
import kotlinx.android.synthetic.main.fragment_friend_list.*


class FriendListFragment :BaseFragment(){
    var adapter: FriendListAdapter?=null


    override fun viewToCreate(): Int {
        return R.layout.fragment_friend_list
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OttoBus.bus.register(this)

        signOut.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            baseAcitivityListener.navigator.openActivity(LoginActivity::class.java)
            activity!!.finish()
            var prefe = UserPreference(activity!!)
            prefe.clearpref()
        }

        friendListRecyclerView.setOnScrollListener(object : HidingScrollListener() {
            override fun onHide() {
                listner!!.updateToolbar(false)
            }

            override fun onShow() {
                listner!!.updateToolbar(true)
            }
        })

    }

   /* @Subscribe
    fun getMessage(list: ArrayList<FriendListBean>?) {

        if (list != null) {
            adapter!!.updateData(list)
            adapter!!.notifyDataSetChanged()
        }

    }*/


    private fun setAadpter(list: MutableList<FriendListBean>) {
        adapter = FriendListAdapter(this!!.activity!!,list)
        friendListRecyclerView.adapter=adapter
    }


    override fun onResume() {
        super.onResume()
        if (activity!!.getAllFriendList()!!.isNotEmpty()) {
            emptyView.visibility=View.GONE
            setAadpter(activity!!.getAllFriendList()!!)
        }
    }


    @SuppressLint("MissingSuperCall")
    override fun onDetach() {
        super.onDetach()
        OttoBus.bus.unregister(this)
    }

}
