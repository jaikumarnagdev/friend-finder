package com.sachtechsolution.friendfinder.ui.splash

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import com.pedro.library.AutoPermissions
import com.pedro.library.AutoPermissionsListener
import com.sachtechsolution.friendfinder.*
import com.sachtechsolution.friendfinder.basepackage.base.BaseActivity
import com.sachtechsolution.friendfinder.database.UserPreference
import com.sachtechsolution.friendfinder.services.GetSavedName
import com.sachtechsolution.friendfinder.services.StoreNaameFromServer
import com.sachtechsolution.friendfinder.ui.otp.activity.LoginActivity
import com.sachtechsolution.friendfinder.view.ui.home.HomeActivity

class SplashScreen : BaseActivity(), AutoPermissionsListener {
    private var logsRunnable: Runnable? = null

    override fun fragmentContainer(): Int {
        return R.id.container
    }
    override fun onGranted(requestCode: Int, permissions: Array<String>) {
        startService(Intent(this, StoreNaameFromServer::class.java))
        storeContacts(getContacts())
        LogOperation().execute()

        Handler().postDelayed({
          if (UserPreference(this).hasUser()) {
                navigator.openActivity(HomeActivity::class.java)
                finish()
            } else {
                navigator.openActivity(LoginActivity::class.java)
                finish()
            }
           // navigator.openActivity(HomeActivity::class.java)
        }, 5000)
    }

    override fun onDenied(requestCode: Int, permissions: Array<String>) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AutoPermissions.loadAllPermissions(this,1)
        setContentView(R.layout.activity_splash_screen)


    }

    private inner class LogOperation() : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            getLogsFromPhone()
            return null
        }

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        AutoPermissions.parsePermissions(this, requestCode, permissions, this)
    }
}
