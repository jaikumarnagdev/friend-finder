package com.sachtechsolution.friendfinder.ui.callDialog

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import kotlinx.android.synthetic.main.call_dialog.*

class CallDialogActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_dialog)
        val dialog = Dialog(this)
        val no = intent.getStringExtra("no")
        val name = ""
        dialog.setContentView(R.layout.call_dialog)
        dialog.contact_no.text = no
        dialog.show()
        if (name.isNullOrEmpty()) {
            FireStoreHelper.getInstance().getContactName(no).addOnCompleteListener {
                if (it.isSuccessful) {
                    var name = "null"
                    it.result.documents.forEach {
                        name = it.id
                    }
                    dialog.contact_name.text = name
                }
            }
        } else {
            dialog.contact_name.text = name
        }
        dialog.setOnCancelListener {
            val intent = Intent(this, CallDialogActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or
                    Intent.FLAG_ACTIVITY_NO_ANIMATION or
                    Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            startActivity(intent)
            finish()
        }
    }
}
