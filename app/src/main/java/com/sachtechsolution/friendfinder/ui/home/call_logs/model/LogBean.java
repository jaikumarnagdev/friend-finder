package com.sachtechsolution.friendfinder.ui.home.call_logs.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LogBean extends RealmObject {

    public LogBean(){

    }
    public LogBean(String name, Long date, int duration,String callernumber){

        this.callername=name;
        this.callDate=date;
        this.callDuration=duration;
        this.callernumber=callernumber;
    }


    public String getCallername() {
        return callername;
    }

    public void setCallername(String callername) {
        this.callername = callername;
    }

    public int getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(int callDuration) {
        this.callDuration = callDuration;
    }

    public Long getCallDate() {
        return callDate;
    }

    public void setCallDate(Long callDate) {
        this.callDate = callDate;
    }

    String callername;
    String callernumber;

    public String getCallernumber() {
        return callernumber;
    }

    public void setCallernumber(String callernumber) {
        this.callernumber = callernumber;
    }

    int callDuration;
    @PrimaryKey
    Long callDate;

}
