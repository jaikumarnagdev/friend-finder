package com.sachtechsolution.friendfinder.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.IBinder
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sachtechsolution.friendfinder.R
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.model.User
import com.sachtechsolution.friendfinder.realm.RealmController
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import com.sachtechsolution.friendfinder.ui.splash.SplashScreen
import io.realm.Sort


/* whoGuri 10/8/18 */
class DialogService : Service() {
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mWindowManager!!.addView(view, params)
        number = intent!!.getStringExtra("number")
        val contactNo = view!!.findViewById<TextView>(R.id.contact_no)
        val contactName = view!!.findViewById<TextView>(R.id.contact_name)
        val contactImage = view!!.findViewById<ImageView>(R.id.dialog_image)
        contactNo.setText(number)

        var realm = RealmController().realm
        val contact= realm.where(ContactsBean::class.java).equalTo("number",number).findFirst()
        if (contact!=null){
            contactName.text = contact.name
        }
        else {
            fireStoreHelper.getUser(number).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result.exists()) {
                        val user = it.result.toObject(User::class.java)
                        contactName.text = user?.name
                        Glide.with(this)
                                .applyDefaultRequestOptions(RequestOptions().placeholder(R.drawable.dumy_user))
                                .load(user?.profile).into(contactImage)
                    } else {
                        fireStoreHelper.getContactName(number).addOnCompleteListener {
                            if (it.isSuccessful) {
                                val docs = it.result.documents
                                val index = docs.size - 1
                                if (index != -1) {
                                    contactName.text = docs[index].id
                                } else
                                    contactName.text = "unknown"
                            } else {
                                contactName.text = "unknown"
                            }
                        }
                    }
                } else {
                    contactName.text = "unknown"
                }
            }
        }
        val dialogMain = view!!.findViewById<LinearLayout>(R.id.dialog_main)
        dialogMain.setOnClickListener {
            startActivity(Intent(this,
                    SplashScreen::class.java).setAction("android.intent.action.MAIN"))
        }
        var close = view!!.findViewById<ImageView>(R.id.close_iv)
        close.setOnClickListener {
            stopSelf()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private var view: View? = null
    var number = ""
    private var mWindowManager: WindowManager? = null

    private var params: WindowManager.LayoutParams? = null

    override fun onCreate() {
        super.onCreate()
        view = LayoutInflater.from(this).inflate(R.layout.call_dialog, null)
        mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)

        params!!.gravity = Gravity.CENTER
        params?.x = 0
        params?.y = 100

        //Add the view to the window

        view!!.setOnTouchListener(object : View.OnTouchListener {
            private var lastAction: Int = 0
            private var initialX: Int = 0
            private var initialY: Int = 0
            private var initialTouchX: Float = 0.toFloat()
            private var initialTouchY: Float = 0.toFloat()

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {

                        //remember the initial position.
                        initialX = params!!.x
                        initialY = params!!.y

                        //get the touch location
                        initialTouchX = event.rawX
                        initialTouchY = event.rawY

                        lastAction = event.action
                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        if (lastAction == MotionEvent.ACTION_DOWN) {
                            //stopSelf()
                        }
                        lastAction = event.action
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        params!!.x = initialX + (event.rawX - initialTouchX).toInt()
                        params!!.y = initialY + (event.rawY - initialTouchY).toInt()

                        mWindowManager!!.updateViewLayout(view, params)
                        lastAction = event.action
                        return true
                    }
                }
                return false
            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        mWindowManager?.removeView(view)
    }
}