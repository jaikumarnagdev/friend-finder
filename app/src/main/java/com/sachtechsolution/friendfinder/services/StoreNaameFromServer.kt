package com.sachtechsolution.friendfinder.services

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.getAllContacts
import com.sachtechsolution.friendfinder.getContacts
import com.sachtechsolution.friendfinder.showToast
import com.sachtechsolution.friendfinder.storeFriendListToRealm
import com.sachtechsolution.friendfinder.ui.home.friendList.model.FriendListBean

class StoreNaameFromServer : Service() {
    var list=ArrayList<FriendListBean>()
    var index=1

    /** indicates how to behave if the service is killed  */
    internal var mStartMode: Int = 0

    /** interface for clients that bind  */
    internal var mBinder: IBinder? = null

    /** indicates whether onRebind should be used  */
    internal var mAllowRebind: Boolean = false

    /** Called when the service is being created.  */
    override fun onCreate() {
        showToast("created")
    }

    /** The service is starting, due to a call to startService()  */
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        showToast("start")
        getFriendToInvite()
        getFriendList(this.getContacts()[index].number)
        return mStartMode
    }

    /** A client is binding to the service with bindService()  */
    override fun onBind(intent: Intent): IBinder? {
        showToast("bind")
        return mBinder
    }

    /** Called when all clients have unbound with unbindService()  */
    override fun onUnbind(intent: Intent): Boolean {
        showToast("unbind")
        return mAllowRebind
    }

    /** Called when a client is binding to the service with bindService() */
    override fun onRebind(intent: Intent) {
        showToast("rebind")

    }
    /** Called when The service is no longer used and is being destroyed  */
    override fun onDestroy() {
        startService(Intent(this,StoreNaameFromServer::class.java))
        showToast("destroy")

    }
    private fun getFriendToInvite() {

        if (index<getAllContacts()!!.size) {
            getFriendList(getAllContacts()!![index].number)
        }

    }

    private fun getFriendList(number: String) {
        var number=number

        if(!number.contains("+")){
            number="+91$number"
        }

        FireStoreHelper.getInstance().getUser(number).addOnCompleteListener {

            if (it.isSuccessful){
                run {
                    if (!it.result.exists())
                    {
                        index= index.plus(1)
                        getFriendToInvite()
                    }
                    else{
                        var name = it.result.data!!["name"].toString()
                        var city = it.result.data!!["city"].toString()
                        var lat = it.result.data!!["lat"].toString()
                        var lon = it.result.data!!["long"].toString()
                        var mobile = it.result.data!!["mobile"].toString()
                        var profile = it.result.data!!["profile"].toString()
                        var state = it.result.data!!["state"].toString()
                        var bean= FriendListBean(name,city,lat,lon,mobile,profile,state)
                        storeFriendListToRealm(bean)
                        index= index.plus(1)
                        getFriendToInvite()
                    }
                }

            }

            else{
                index =index.plus(1)
                getFriendToInvite()
            }
        }

    }

}
