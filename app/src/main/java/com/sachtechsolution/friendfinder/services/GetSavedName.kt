package com.sachtechsolution.friendfinder.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.sachtechsolution.friendfinder.bus.OttoBus
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.getLogFromRealm
import com.sachtechsolution.friendfinder.showToast
import com.sachtechsolution.friendfinder.storeContacts
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean

class GetSavedName:Service()
    {
        var list:ArrayList<ContactsBean>?=null
    /** indicates how to behave if the service is killed  */
    internal var mStartMode: Int = 0

    /** interface for clients that bind  */
    internal var mBinder: IBinder? = null
    var index =1
    /** indicates whether onRebind should be used  */
    internal var mAllowRebind: Boolean = false

    /** Called when the service is being created.  */
    override fun onCreate() {
        showToast("created")
    }

    /** The service is starting, due to a call to startService()  */
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        showToast("start")
        searchNamae()
        return mStartMode
    }


    /** A client is binding to the service with bindService()  */
    override fun onBind(intent: Intent): IBinder? {
        showToast("bind")
        return mBinder
    }

    /** Called when all clients have unbound with unbindService()  */
    override fun onUnbind(intent: Intent): Boolean {
        showToast("unbind")
        return mAllowRebind
    }

    /** Called when a client is binding to the service with bindService() */
    override fun onRebind(intent: Intent) {
        showToast("rebind")

    }

    /** Called when The service is no longer used and is being destroyed  */
    override fun onDestroy() {
        showToast("destroy")

    }
    private fun searchNamae() {

        if (index<getLogFromRealm()!!.size) {
            val callername = getLogFromRealm()!![index].callername
            if (!callername.contains("+"))
            {
                getFriendList(callername,index)
            }else{
                this.index =index.plus(1)
                searchNamae()
            }

        }else{
            storeContacts(this!!.list!!)
            showToast("done")
        }

    }

    private fun getFriendList(number: String, index: Int) {
        var number = number

        if (!number.contains("+")) {
            number = "+91$number"
        }

        FireStoreHelper.getInstance().getContactName(number).addOnCompleteListener {
            if (it.isSuccessful) {
                var name = ""
                it.result.documents.forEach {
                    name = it.id
                    showToast(name)
                    OttoBus.bus.post("$name,$index")
                    this.index= index.plus(1)
                    var bean=ContactsBean(name,"noImage",number)
                    list!!.add(bean)
                    searchNamae()
                }
            }

            else{
                this.index =index.plus(1)
                searchNamae()
            }

        }

        }

    }