package com.sachtechsolution.friendfinder.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.realm.RealmController
import com.sachtechsolution.friendfinder.showToast
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import io.realm.Realm
import java.util.ArrayList

class CallReceiver : BroadcastReceiver() {
    var realm: Realm?=null

    override fun onReceive(context: Context, intent: Intent) {

        realm = RealmController().realm
        //Log.w("intent " , intent.getAction().toString());
        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.action == "android.intent.action.NEW_OUTGOING_CALL") {
            val savedNumber = intent.extras!!.getString("android.intent.extra.PHONE_NUMBER")

        } else {
            val number = intent.extras!!.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)
            FireStoreHelper.getInstance().getContactName(number).addOnCompleteListener {
                if (it.isSuccessful) {
                    var name = ""
                    it.result.documents.forEach {
                        name = it.id
                    }
                    if (!name.isEmpty()){
                        context.showToast(name)
                        var list=ArrayList<ContactsBean>()
                        var bean=ContactsBean(name,"noImage",number)
                        list.add(bean)
                        storeContacts(list)
                    }
                }
            }
        }
    }

    fun storeContacts(contactsBeas: ArrayList<ContactsBean>) {
        realm!!.beginTransaction()
        realm!!.insertOrUpdate(contactsBeas)
        realm!!.commitTransaction()
    }
    companion object {
        private val lastState = TelephonyManager.CALL_STATE_IDLE
    }
}