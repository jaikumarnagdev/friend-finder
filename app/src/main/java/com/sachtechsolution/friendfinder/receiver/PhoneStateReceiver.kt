package com.sachtechsolution.friendfinder.receiver

/* whoGuri 10/8/18 */

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.TelephonyManager
import com.sachtechsolution.friendfinder.services.DialogService
import com.sachtechsolution.friendfinder.ui.callDialog.CallDialogActivity


//

class PhoneStateReceiver : BroadcastReceiver() {

    private var running: Boolean = false

    @SuppressLint("MissingPermission")
    override fun onReceive(context: Context?, intent: Intent?) {
        //Toast.makeText(context,"receiver",Toast.LENGTH_SHORT).show()
        val no = intent?.extras?.getString("incoming_number")
        val manager = context?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        var list = manager!!.getRunningServices(Integer.MAX_VALUE)

        /*for (service in list) {
            if ("com.sachtechsolution.friendfinder.services.DialogService".equals(service.service.className)) {
                running=true
                break
            }
        }
        if (!running)*/

        context!!.startService(Intent(context, DialogService::class.java).putExtra("number", no))

            context!!.startService(Intent(context,DialogService::class.java).putExtra("number",no))

        try {
            if (!(context as Activity).isFinishing)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    (context as Activity).finishAndRemoveTask()
                }
            val state = intent?.getStringExtra(TelephonyManager.EXTRA_STATE)
            if (state == TelephonyManager.EXTRA_STATE_RINGING) {
                //Toast.makeText(context,"ringing",Toast.LENGTH_SHORT).show()
                //context.startService(Intent(context,DialogService::class.java))
                //openDialog(context, no)

            }
            if (state == TelephonyManager.EXTRA_STATE_OFFHOOK) {
                //Toast.makeText(context,"offHook",Toast.LENGTH_SHORT).show()
                context.stopService(Intent(context, DialogService::class.java))

                //openDialog(context, no)
            }
            if (state == TelephonyManager.EXTRA_STATE_IDLE) {
                //Toast.makeText(context,"idle",Toast.LENGTH_SHORT).show()
                context.stopService(Intent(context, DialogService::class.java))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun openDialog(context: Context?, no: String?) {
        val callIntent = Intent("android.intent.action.MAIN")
        callIntent.setClass(context, CallDialogActivity::class.java)
        callIntent.putExtra("no", no)
        callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context?.startActivity(callIntent)
    }
}


