package com.sachtechsolution.friendfinder.receiver

import android.annotation.SuppressLint
import android.app.Activity
import android.telephony.TelephonyManager
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.os.Build
import com.sachtechsolution.friendfinder.database.FireStoreHelper
import com.sachtechsolution.friendfinder.realm.RealmController
import com.sachtechsolution.friendfinder.showToast
import com.sachtechsolution.friendfinder.ui.callDialog.CallDialogActivity
import com.sachtechsolution.friendfinder.ui.home.contacts.model.ContactsBean
import io.realm.Realm
import java.util.ArrayList

//

class OutgoingCallReceiver : BroadcastReceiver() {
    var realm: Realm?=null
    @SuppressLint("MissingPermission")
    override fun onReceive(context: Context?, intent: Intent?) {
        realm = RealmController().realm
        try {
            val state = intent?.getStringExtra(TelephonyManager.EXTRA_STATE)
            val number = intent?.extras?.getString("incoming_number")

            FireStoreHelper.getInstance().getContactName(number!!).addOnCompleteListener {

                if (it.isSuccessful) {
                    var name = ""
                    it.result.documents.forEach {
                        name = it.id
                    }
                    if (!name.isEmpty()) {
                        var list = ArrayList<ContactsBean>()
                        var bean = ContactsBean(name, "noImage", number)
                        list.add(bean)
                        storeContacts(list)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun openDialog(context: Context?, no: String?) {
        val callIntent = Intent("android.intent.action.MAIN")
        callIntent.setClass(context, CallDialogActivity::class.java)
        callIntent.putExtra("no", no)
        callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context?.startActivity(callIntent)
    }

    fun storeContacts(contactsBeas: ArrayList<ContactsBean>) {
        realm!!.beginTransaction()
        realm!!.insertOrUpdate(contactsBeas)
        realm!!.commitTransaction()
    }
}


