package com.sachtechsolution.friendfinder.util

import android.Manifest

//
object Constants{
    var LONGINED=true
    val PERMISSIONS= arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_PHONE_STATE)
}