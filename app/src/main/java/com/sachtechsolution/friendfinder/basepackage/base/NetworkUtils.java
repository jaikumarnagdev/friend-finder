package com.sachtechsolution.friendfinder.basepackage.base;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

public class NetworkUtils {
    public static boolean isNetworkAvailable(@NonNull Context context) {
        if (context != null) {
            NetworkInfo activeNetwork = ((ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        }
        return false;
    }
}
