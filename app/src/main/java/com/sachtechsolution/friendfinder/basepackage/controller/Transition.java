package com.sachtechsolution.friendfinder.basepackage.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public interface Transition {

    void performTransiton(FragmentTransaction transaction, int container, Fragment baseFragment);
}
