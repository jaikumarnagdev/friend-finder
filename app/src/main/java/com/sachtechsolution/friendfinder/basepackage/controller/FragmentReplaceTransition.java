package com.sachtechsolution.friendfinder.basepackage.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;



/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public class FragmentReplaceTransition implements Transition {

    @Override
    public void performTransiton(FragmentTransaction transaction, int container, Fragment baseFragment) {
        transaction.replace(container, baseFragment);
    }
}
