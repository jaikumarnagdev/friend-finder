package com.sachtechsolution.friendfinder.basepackage.base;

import com.sachtechsolution.friendfinder.basepackage.controller.Navigator;


/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public interface BaseAcitivityListener {

     Navigator getNavigator();

}
