package com.sachtechsolution.friendfinder.basepackage.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import gurtek.mrgurtekbasejava.R;


/**
 * Created by Sachtech on 1/6/2018.
 */

public class ProgressDialogFragment extends DialogFragment {
   static ProgressDialogFragment progressDialogFragment;

    public static ProgressDialogFragment newInstance(String title) {

        Bundle args = new Bundle();
        args.putString("message",title);
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return inflater.inflate(R.layout.progress_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.message)).setText(getArguments().getString("message"));
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
    }
    public static void  showProgress(FragmentManager fragmentManager, String Title){
         progressDialogFragment=ProgressDialogFragment.newInstance(Title);
        progressDialogFragment.show(fragmentManager,ProgressDialogFragment.class.getName());
    }
    public static void  showProgress(FragmentManager fragmentManager){
        progressDialogFragment=ProgressDialogFragment.newInstance("");
        progressDialogFragment.show(fragmentManager,ProgressDialogFragment.class.getName());
    }
    public static void hideProgress(){
        if(progressDialogFragment!=null)
        progressDialogFragment.dismiss();
    }
}
