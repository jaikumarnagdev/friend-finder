package com.sachtechsolution.friendfinder.model

data class User(
    var name: String = "",
    var mobile: String = "",
    var lat: String = "",
    var long: String = "",
    var state: String = "",
    var city: String = "",
    var profile: String = ""
)

