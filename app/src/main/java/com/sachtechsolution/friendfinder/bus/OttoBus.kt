package com.sachtechsolution.friendfinder.bus

import com.squareup.otto.Bus

object OttoBus {
    private var sBus: Bus? = null
    val bus: Bus
        get() {
            if (sBus == null)
                sBus = Bus()
            return sBus!!
        }
}
