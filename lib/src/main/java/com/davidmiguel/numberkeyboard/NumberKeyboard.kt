package com.davidmiguel.numberkeyboard

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.support.annotation.AttrRes
import android.support.annotation.ColorRes
import android.support.annotation.Dimension
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.GridLayout
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import java.util.ArrayList

/**
 * Number keyboard (to enter pin or custom amount).
 */
class NumberKeyboard : GridLayout {

    @Dimension
    private var keyWidth: Int = 0
    @Dimension
    private var keyHeight: Int = 0
    @DrawableRes
    private var numberKeyBackground: Int = 0
    @Dimension
    private var numberKeyTextSize: Int = 0
    @ColorRes
    private var numberKeyTextColor: Int = 0
    @DrawableRes
    private var leftAuxBtnIcon: Int = 0
    @DrawableRes
    private var leftAuxBtnBackground: Int = 0
    @DrawableRes
    private var rightAuxBtnIcon: Int = 0
    @DrawableRes
    private var rightAuxBtnBackground: Int = 0

    private var numericKeys: MutableList<TextView>? = null
    private var leftAuxBtn: ImageView? = null
    private var rightAuxBtn: ImageView? = null

    private var listener: NumberKeyboardListener? = null

    constructor(context: Context) : super(context) {
        inflateView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initializeAttributes(attrs)
        inflateView()
    }

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initializeAttributes(attrs)
        inflateView()
    }

    /**
     * Sets keyboard listener.
     */
    fun setListener(listener: NumberKeyboardListener) {
        this.listener = listener
    }

    /**
     * Hides left auxiliary button.
     */
    fun hideLeftAuxButton() {
        leftAuxBtn!!.visibility = View.GONE
    }

    /**
     * Shows left auxiliary button.
     */
    fun showLeftAuxButton() {
        leftAuxBtn!!.visibility = View.VISIBLE
    }

    /**
     * Hides right auxiliary button.
     */
    fun hideRightAuxButton() {
        rightAuxBtn!!.visibility = View.GONE
    }

    /**
     * Shows right auxiliary button.
     */
    fun showRightAuxButton() {
        rightAuxBtn!!.visibility = View.VISIBLE
    }

    /**
     * Sets key width in px.
     */
    fun setKeyWidth(px: Int) {
        for (key in numericKeys!!) {
            key.layoutParams.width = px
        }
        leftAuxBtn!!.layoutParams.width = px
        rightAuxBtn!!.layoutParams.width = px
        requestLayout()
    }

    /**
     * Sets key height in px.
     */
    fun setKeyHeight(px: Int) {
        for (key in numericKeys!!) {
            key.layoutParams.height = px
        }
        leftAuxBtn!!.layoutParams.height = px
        rightAuxBtn!!.layoutParams.height = px
        requestLayout()
    }

    /**
     * Sets number keys background.
     */
    fun setNumberKeyBackground(@DrawableRes background: Int) {
        for (key in numericKeys!!) {
            key.background = ContextCompat.getDrawable(context, background)
        }
    }

    /**
     * Sets number keys text size.
     */
    fun setNumberKeyTextSize(@Dimension size: Int) {
        for (key in numericKeys!!) {
            key.setTextSize(TypedValue.COMPLEX_UNIT_PX, size.toFloat())
        }
    }

    /**
     * Sets number keys text color.
     */
    fun setNumberKeyTextColor(@ColorRes color: Int) {
        for (key in numericKeys!!) {
            key.setTextColor(ContextCompat.getColorStateList(context, color))
        }
    }

    /**
     * Sets number keys text typeface.
     */
    fun setNumberKeyTypeface(typeface: Typeface) {
        for (key in numericKeys!!) {
            key.typeface = typeface
        }
    }

    /**
     * Sets left auxiliary button icon.
     */
    fun setLeftAuxButtonIcon(@DrawableRes icon: Int) {
        leftAuxBtn!!.setImageResource(icon)
    }

    /**
     * Sets right auxiliary button icon.
     */
    fun setRightAuxButtonIcon(@DrawableRes icon: Int) {
        rightAuxBtn!!.setImageResource(icon)
    }

    /**
     * Sets left auxiliary button background.
     */
    fun setLeftAuxButtonBackground(@DrawableRes bg: Int) {
        leftAuxBtn!!.background = ContextCompat.getDrawable(context, bg)
    }

    /**
     * Sets right auxiliary button background.
     */
    fun setRightAuxButtonBackground(@DrawableRes bg: Int) {
        rightAuxBtn!!.background = ContextCompat.getDrawable(context, bg)
    }

    /**
     * Initializes XML attributes.
     */
    private fun initializeAttributes(attrs: AttributeSet?) {
        val array = context.theme.obtainStyledAttributes(
                attrs, R.styleable.NumberKeyboard, 0, 0)
        try {
            // Get keyboard type
            val type = array.getInt(R.styleable.NumberKeyboard_keyboardType, -1)
            if (type == -1) {
                throw IllegalArgumentException("keyboardType attribute is required.")
            }
            // Get key sizes
            keyWidth = array.getLayoutDimension(R.styleable.NumberKeyboard_keyWidth,
                    dpToPx(DEFAULT_KEY_WIDTH_DP.toFloat()))
            keyHeight = array.getLayoutDimension(R.styleable.NumberKeyboard_keyHeight,
                    dpToPx(DEFAULT_KEY_HEIGHT_DP.toFloat()))
            // Get number key background
            numberKeyBackground = array.getResourceId(R.styleable.NumberKeyboard_numberKeyBackground,
                    R.drawable.key_bg)
            // Get number key text size
            numberKeyTextSize = array.getDimensionPixelSize(R.styleable.NumberKeyboard_numberKeyTextSize,
                    spToPx(DEFAULT_KEY_TEXT_SIZE_SP.toFloat()))
            // Get number key text color
            numberKeyTextColor = array.getResourceId(R.styleable.NumberKeyboard_numberKeyTextColor,
                    R.drawable.key_text_color)
            // Get auxiliary icons
            when (type) {
                0 // integer
                -> {
                    leftAuxBtnIcon = R.drawable.key_bg_transparent
                    rightAuxBtnIcon = R.drawable.ic_backspace
                    leftAuxBtnBackground = R.drawable.key_bg_transparent
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                }
                1 // decimal
                -> {
                    leftAuxBtnIcon = R.drawable.ic_comma
                    rightAuxBtnIcon = R.drawable.ic_backspace
                    leftAuxBtnBackground = R.drawable.key_bg
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                }
                2 // fingerprint
                -> {
                    leftAuxBtnIcon = R.drawable.ic_fingerprint
                    rightAuxBtnIcon = R.drawable.ic_backspace
                    leftAuxBtnBackground = R.drawable.key_bg_transparent
                    rightAuxBtnBackground = R.drawable.key_bg_transparent
                }
                3 // custom
                -> {
                    leftAuxBtnIcon = array.getResourceId(R.styleable.NumberKeyboard_leftAuxBtnIcon,
                            R.drawable.key_bg_transparent)
                    rightAuxBtnIcon = array.getResourceId(R.styleable.NumberKeyboard_rightAuxBtnIcon,
                            R.drawable.key_bg_transparent)
                    leftAuxBtnBackground = array.getResourceId(R.styleable.NumberKeyboard_leftAuxBtnBackground,
                            R.drawable.key_bg_transparent)
                    rightAuxBtnBackground = array.getResourceId(R.styleable.NumberKeyboard_rightAuxBtnBackground,
                            R.drawable.key_bg_transparent)
                }
                else -> {
                    leftAuxBtnIcon = R.drawable.key_bg_transparent
                    rightAuxBtnIcon = R.drawable.key_bg_transparent
                    leftAuxBtnBackground = R.drawable.key_bg
                    rightAuxBtnBackground = R.drawable.key_bg
                }
            }
        } finally {
            array.recycle()
        }
    }

    /**
     * Inflates layout.
     */
    private fun inflateView() {
        val view = View.inflate(context, R.layout.number_keyboard, this)
        // Get numeric keys
        numericKeys = ArrayList(10)
        numericKeys!!.add(view.findViewById<View>(R.id.key0) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key1) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key2) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key3) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key4) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key5) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key6) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key7) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key8) as TextView)
        numericKeys!!.add(view.findViewById<View>(R.id.key9) as TextView)
        // Get auxiliary keys
        leftAuxBtn = view.findViewById(R.id.leftAuxBtn)
        rightAuxBtn = view.findViewById(R.id.rightAuxBtn)
        // Set styles
        setStyles()
        // Set listeners
        setupListeners()
    }

    /**
     * Set styles.
     */
    private fun setStyles() {
        setKeyWidth(keyWidth)
        setKeyHeight(keyHeight)
        setNumberKeyBackground(numberKeyBackground)
        setNumberKeyTextSize(numberKeyTextSize)
        setNumberKeyTextColor(numberKeyTextColor)
        setLeftAuxButtonIcon(leftAuxBtnIcon)
        setLeftAuxButtonBackground(leftAuxBtnBackground)
        setRightAuxButtonIcon(rightAuxBtnIcon)
        setRightAuxButtonBackground(rightAuxBtnBackground)
    }

    /**
     * Setup on click listeners.
     */
    private fun setupListeners() {
        // Set number callbacks
        for (i in numericKeys!!) {

            i.setOnClickListener {
                if (listener != null) {
                    listener!!.onNumberClicked(i.text.toString().toInt())
                }
            }
        }
        // Set auxiliary key callbacks
        leftAuxBtn!!.setOnClickListener {
            if (listener != null) {
                listener!!.onLeftAuxButtonClicked()
            }
        }
        rightAuxBtn!!.setOnClickListener {
            if (listener != null) {
                listener!!.onRightAuxButtonClicked()
            }
        }
    }


    /**
     * Utility method to convert dp to pixels.
     */
    fun dpToPx(valueInDp: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, resources.displayMetrics).toInt()
    }

    /**
     * Utility method to convert sp to pixels.
     */
    fun spToPx(valueInSp: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, valueInSp, resources.displayMetrics).toInt()
    }

    companion object {

        private val DEFAULT_KEY_WIDTH_DP = 70
        private val DEFAULT_KEY_HEIGHT_DP = 70
        private val DEFAULT_KEY_TEXT_SIZE_SP = 32

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}
