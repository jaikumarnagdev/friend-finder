package gurtek.mrgurtekbasejava.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public abstract class BaseFragment extends Fragment {
   public ModelPrefrence pref;

    protected static BaseAcitivityListener baseAcitivityListener;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseAcitivityListener) {
            baseAcitivityListener = (BaseAcitivityListener) context;
            pref=new ModelPrefrence(context);
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(viewToCreate(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = new ModelPrefrence(getActivity());
    }

    @LayoutRes
    protected abstract int viewToCreate();

    public void showProgress() {
        ProgressDialogFragment.showProgress(getActivity().getSupportFragmentManager());
    }

    public void showProgress(String s) {
        ProgressDialogFragment.showProgress(getActivity().getSupportFragmentManager(), s);
    }

    public void hideProgress() {
        ProgressDialogFragment.hideProgress();
    }

    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showLog(String message, String value) {
        Log.e(message, value);
    }

    public boolean networkAvaileble() {
        if (NetworkUtils.isNetworkAvailable(getActivity())) {

            return true;
        } else {
            return false;
        }
    }
}
