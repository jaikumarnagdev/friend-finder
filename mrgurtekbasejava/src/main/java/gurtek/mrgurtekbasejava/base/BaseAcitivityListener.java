package gurtek.mrgurtekbasejava.base;

import android.app.Activity;

import gurtek.mrgurtekbasejava.controller.Navigator;

/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public interface BaseAcitivityListener {

     Navigator getNavigator();

}
