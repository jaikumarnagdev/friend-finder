package gurtek.mrgurtekbasejava.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;
import gurtek.mrgurtekbasejava.controller.FragmentAddTransition;
import gurtek.mrgurtekbasejava.controller.FragmentReplaceTransition;
import gurtek.mrgurtekbasejava.controller.Navigator;

/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseAcitivityListener {
    public ModelPrefrence pref;
    private int container = fragmentContainer();
    protected Navigator navigator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectNavigator();
        pref = new ModelPrefrence(this);

    }

    private void injectNavigator() {
        navigator = new Navigator(this, container, new FragmentAddTransition(), new FragmentReplaceTransition());
    }

    /**
     * @return navigator to navigate from one screen to another
     */
    @Override
    public Navigator getNavigator() {
        return navigator;
    }


    /**
     * @return container on which you want to inflate fragment
     * it will be 0 if no fragment is in activity
     */
    public abstract int fragmentContainer();

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()==1)
            finish();
        else super.onBackPressed();

    }public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }public void showLog(String message,String value){
        Log.e(message,value);
    }public void showProgress(){
        ProgressDialogFragment.showProgress(this.getSupportFragmentManager());
    }public void showProgress(String s){
        ProgressDialogFragment.showProgress(this.getSupportFragmentManager(),s);
    }public void hideProgress(){
        ProgressDialogFragment.hideProgress();
    }
    public boolean networkAvaileble(){
        if (NetworkUtils.isNetworkAvailable(this)) {
            return true;
        }else {
            return false;
        }

    }
}
