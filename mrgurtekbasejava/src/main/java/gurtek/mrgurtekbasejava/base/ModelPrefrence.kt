package gurtek.mrgurtekbasejava.base

import android.content.Context
import android.content.SharedPreferences

class ModelPrefrence(context: Context) {
    private var timeStemp: String?=null
    private var preferences: SharedPreferences = context.applicationContext.getSharedPreferences("MyPref", 0)
    private var editor: SharedPreferences.Editor

    init {
        editor = preferences.edit()

    }


    fun getLasttimeStemp(): String? {
        return preferences.getString(TIMESTEMP, null)
    }

    fun setLasttimeStemp(phoneNumber: String) {
        this.timeStemp = phoneNumber
        editor.putString(TIMESTEMP, phoneNumber)
        editor.apply()
        editor.commit()
    }

    companion object {

        var TIMESTEMP = "timeStemp"
    }
}